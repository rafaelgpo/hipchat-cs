﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipchatApiV2.Responses
{
    public class HipchatGetPrivateMessageHistory
    {
        public List<HipchatMessage> items { get; set; }
        public HipchatLink links { get; set; }
        public int startIndex { get; set; }
        public int maxResults { get; set; }
    }
}
