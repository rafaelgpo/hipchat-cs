﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipchatApiV2.Responses
{
    public class HipchatMessage
    {
        public Guid id { get; set; }
        public DateTime date { get; set; }
        public string message { get; set; }
        public HipchatUser from { get; set; }
    }
}
