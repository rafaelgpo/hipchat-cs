﻿namespace IntegrationTests
{
    public static class TestsConfig
    {
        public static string AuthToken = "YTaZZ1pgoeJzTktcEHxgLTqAY0R4XFEJx0YFk4WZ";
        public static int ExistingRoomId = 791471;
        public static int ExistingUserId = 1181788;
    }
}