﻿using HipchatApiV2;
using HipchatApiV2.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace IntegrationTests
{
    [Trait("SendPrivateMessage", "")]
    public class SendPrivateMessage : IDisposable
    {
        private readonly int _existingUserId;
        private readonly HipchatClient _client;
        
        public SendPrivateMessage()
        {
            HipchatApiConfig.AuthToken = TestsConfig.AuthToken;
            _client = new HipchatClient();

            string userLogin = "user.mail@test.com";
            string userPassword = "12345678";

            HipchatApiConfig.AuthToken = _client.GenerateUserToken(GrantType.Password, new List<TokenScope> { TokenScope.SendMessage }, userLogin, userPassword).Access_Token;
            _client = new HipchatClient();
        }

        [Fact(DisplayName = "Can send a private message")]
        public void CanSendPrivateMessage()
        {
            var sendMessageResult = _client.SendPrivateMessage(TestsConfig.ExistingUserId.ToString(), "Testando!!!");

            Assert.True(sendMessageResult);
        }



        public void Dispose()
        {
        }
    }
}
